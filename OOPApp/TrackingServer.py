__author__ = 'Gena'

from StereoTracker import StereoTracker
import numpy as np
import socket
from dprint import dprint
import pickle

class TrackingServer():
    '''Tracking server is  a tracker and a server. (Stereo tracker)


    This baby should process the following requests
    set_origin: init the reference coordinate system
    restart: restart the tracker, optionally flush the state
    close: stop the tracker and connection
    shutdown: shutdown the server
    coords : returns raw ball coords
    RT: returns relative RT between balls 0,1,2 and 3,4,5
    project: returns u,v for each cam, given x,y,z'''
    def __init__(self,n = 3,port = 12345):

        self.n = n
        self.port = port
        self.xyz = None
        self.SIGTERM = False #this is set to True to escape the main loop and terminate, leeeet
        self.is_paused = False
        self.origin = np.zeros((3,1))
        self.i  = np.array([1,0,0])
        self.j  = np.array([0,1,0])
        self.k  = np.array([0,0,1])

        self.R = np.eye(3,3)
        self.T = np.array([0,0,0])[:,np.newaxis]

        self._start_tracker()

    #################################
    #UPDAte coords

    def _update_coords(self):
        self.xyz = self.st.track()
        self.T = self.xyz[:,0][:,np.newaxis]-self.origin #translation in camera frame

        self.xyz = self.xyz-self.xyz[:,0][:,np.newaxis] #"demean", i.e. bring reference point to 0,0,0 for rotation calcs

        self.ii = self.xyz[:,1]/np.linalg.norm(self.xyz[:,1])
        self.jj = self.xyz[:,2]/np.linalg.norm(self.xyz[:,2])

        self.kk = np.cross(self.ii,self.jj)
        self.kk = self.kk/np.linalg.norm(self.kk)


        self.iijjkk = np.concatenate((self.ii[:,np.newaxis],self.jj[:,np.newaxis],self.kk[:,np.newaxis]), axis = 1)


        self.xx = np.dot(self.iijjkk.transpose(),self.i)
        self.yy = np.dot(self.iijjkk.transpose(),self.j)
        self.zz = np.dot(self.iijjkk.transpose(),self.k)

        self.RR = np.concatenate(([self.xx],[self.yy],[self.zz]))

        try:
            # project to nearest orthonormal matrix
            M = np.matrix(self.RR)
            l,e = np.linalg.eig(M.transpose()*M)
            MtMsqrt = 1/np.sqrt(l[0]) * e[:,0] * e[:,0].transpose() + \
                      1/np.sqrt(l[1]) * e[:,1] * e[:,1].transpose() + \
                      1/np.sqrt(l[2]) * e[:,2] * e[:,2].transpose()
            self.RR = M * MtMsqrt
        except:
            pass



        # /np.sqrt(l)
        # SQR_SOMETHING =

        # self.TT = np.array([np.dot(self.T.transpose(),basis_vector) for basis_vector in (self.i,self.j,self.k)])[:,np.newaxis]

        self.R = np.matrix(np.array([self.i,self.j,self.k]))
        self.TT = self.R*self.T

        #coordinates in new coordinate frame
        # self.I =  np.array([np.dot(self.xyz[:,1],basis_vector) for basis_vector in (self.i,self.j,self.k)])[:,np.newaxis]+self.TT
        # self.J =  np.array([np.dot(self.xyz[:,2],basis_vector) for basis_vector in (self.i,self.j,self.k)])[:,np.newaxis]+self.TT

        self.I = self.R*self.xyz[:,1][:,np.newaxis]
        self.J = self.R*self.xyz[:,2][:,np.newaxis]

        self.xyz = np.concatenate((self.TT,self.I+self.TT,self.J+self.TT),axis = 1)
        self.xyz = np.array(self.xyz.transpose())
        if np.isnan(np.sum(self.xyz)):
            pass





    ###############################################################
    # THESE METHODS ARE REQUESTS THAT COULD BE PROCESSED BY THE SERVER
    ###############################################################

    def shutdown(self,**kwargs):
        try:
            self._stop_tracker()
        finally:
            self.SIGTERM = True
            return {'error':'0',
                'message':'the server is shutting down'}


    def old_coords(self,**kwargs):
        outstring =''
        for i in range(self.st.n):
            outstring += ','.join(['%.1f' % element for element in self.xyz[:,i]-self.origin[:,i]])
            outstring += ','
        outstring =outstring[:-1]
        response = {'error':'0',
                    'message':outstring}
        return response

    def coords(self,**kwargs):
        response = {'error':'0',
            'message':self.xyz}
        return response


    def set_origin(self,**kwargs):
        self.xyz = self.st.track()
        self.origin = self.xyz[:,0][:,np.newaxis]
        self.xyz = self.xyz-self.origin
        self.i = self.xyz[:,1]
        self.i = self.i/np.linalg.norm(self.i)
        self.j = self.xyz[:,2]
        self.j = self.j/np.linalg.norm(self.j)
        self.k = np.cross(self.i,self.j)
        self.k = self.k/np.linalg.norm(self.k)

        # dprint('norm i %s' % np.linalg.norm(self.i))
        # dprint('norm j %s' % np.linalg.norm(self.j))
        # dprint('norm k %s' % np.linalg.norm(self.k))
        #
        # dprint('angle ij %f' % np.degrees(np.arccos(np.dot(self.i,self.j))))
        # dprint('angle ik %s' % np.degrees(np.arccos(np.dot(self.i,self.k))))
        # dprint('angle jk %s' % np.degrees(np.arccos(np.dot(self.j,self.k))))

        response = {'error':'0',
                    'message': 'origin set to %s\n i,j,k = %s, %s, %s' % (self.i, self.j, self.k)
                    }
        return response

    def RT(self,**kwargs):
        response = {'error':'0',
                       'R':self.RR,
                       'T':self.TT}

        return response


    def reinitialise_tracking(self,**kwargs):
        self.st.close()
        self.st = StereoTracker(n = self.n, use_init_gui= True)
        self.st.update()
        response = {'error':'0',
                       'message':'tracking reinitialization complete (gui on the server)'
                    }
        return response

    def pause(self,**kwargs):
        self.is_paused = True
        response = {'error':'0',
                       'message':'tracking paused'
                    }

    def resume(self,**kwargs):
        self.is_paused = False
        response = {'error':0,
                    'message': 'tracking resumed'}


    def save_state(self,**kwargs):
        res = self.st.save_state()
        if not res:
            response = {'error':'401',
                        'message':'could not save state, probably due to permissions'}
        else:
            response = {'error':'0',
                        'message':'tracking state saved'}
        return response







    ##############################
    # THESE ARE NOT REQUESTS, BUT GUTS, DISTINGUISHED BY THEIR FANCY LEADING UNDERSCORE
    ###############################

    # def _ijk(self): ## LEFT HANDED!
    #     self.i =

    def _start_tracker(self):

        self.st = StereoTracker(n = self.n)
        self.st.update()
        self.xyz = self.st.track()

    def _stop_tracker(self):
        self.st.close()
        self.st = None
        self.xyz = None

    def _process(self,request):
        '''
        process the requests from clients
        :return:
        '''
        decoded_dic  = self._decode_message(request)
        try: #too lazy to do it properly, sry
            function_name = decoded_dic.pop('function','_no_function_specified_error_handler')
            f =getattr(self,function_name) # get pointer to the relevant function and delete it from the dict
            if function_name[0] == '_' or function_name=='st':
                response = {'error':'1337',
                            'message':'go hack yourself'}
                return self._encode_message(response)

            response = f(**decoded_dic) # call the sucker

        except:
            response = {'error':'500',
                        'message':'could not process your message'}
        return self._encode_message(response)

    def _no_function_specified_error_handler(self,**kwargs):
        response = {'error':'1',
                    'message':'no function in your request, I did nothing'}
        return self._encode_message(response)


    #--------------------------------------------------------------------------
    # SOCKET COMMUNICATION PROTOCOL #Stolen and adapted ^w^w^w Backported from Ziga^w^w^w : PICKLE!!
    #--------------------------------------------------------------------------
    def _decode_message(self, data_bytes):
        return pickle.loads(data_bytes)


    def _encode_message(self, data_dict):
        return pickle.dumps(data_dict,2)







 #--------------------------------------------------------------------------
    # MAIN LOOP
    #--------------------------------------------------------------------------

    def _run(self):

        # INIT SERVER
        s = socket.socket()         # Create a socket object
        host = socket.gethostname() # Get local machine name
        port = self.port                # Reserve a port for your service.
        s.bind((host, port))        # Bind to the port
        s.setblocking(0)            # IMPORTANT: enables us being a single-threaded servah!
        s.listen(5)                 # Now wait for client connection.
        message = []

        # c, addr = s.accept()     # It was here for debuggin purposes
        while not self.SIGTERM:
            try: # the nonblocking socket accept will raie an exception in no-one connects
                c, addr = s.accept()     # Establish connection with client. This is blocking!
                # dprint('CONNECTED!!!')
                dprint('Got connection from %s' % (addr,))
                request = c.recv(1024)
                response = self._process(request)
                c.sendall(response)

            # except Exception as ex:
            #     template = "An exception of type {0} occured. Arguments:\n{1!r}"
            #     message = template.format(type(ex).__name__, ex.args)
            #     print message
            #     dprint('no connectah :('except Exception as ex:
            except socket.error:
                pass
                # dprint('Gotcha')
                # dprint('not connected :(')
            finally: #track anyway!
                if not self.is_paused:
                    self.st.update()
                    self._update_coords()




        return 0 #ohhhh, sweet zero


if __name__ == '__main__':
    serv = TrackingServer()
    serv._run()


