__author__ = 'Gena'

import socket
import pickle

class Client(object):

    def __init__(self,host = 'localhost', port = 12345):
        self._address = host
        self._port = port
    #--------------------------------------------------------------------------
    # SOCKET COMMUNICATION PROTOCOL #Stolen and adapted ^w^w^w Backported from Ziga
    #--------------------------------------------------------------------------
    def _decode_message(self, data_bytes):
        return pickle.loads(data_bytes)


    def _encode_message(self, data_dict):
        return pickle.dumps(data_dict)


    def _exchange_messages( self, message, address='localhost', port=11000 ):
        """Exchange message-response over socket interface"""
        # create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # connect the socket to the port where the server is listening
        server_address = (self._address, self._port)
        sock.connect(server_address)
        # set default response
        response = {}
        try:
            # send data
#            dprint("message: %s" % message)
#            dprint("self._encode_message(message): %s " % self._encode_message(message))
            sock.sendall( self._encode_message(message) )
#            dprint("Start receiving...")
            # look for the response
            data_bytes = sock.recv(1024)
            # decode message
#            dprint( "data_bytes: %s" % data_bytes )
            response = self._decode_message( data_bytes )
#            dprint( "response: %s" % response )
        finally:
            # close socket
            sock.close()

            # return response
            return response

        return bytes(data_bytes, encoding)


if __name__=='__main__':


    host = socket.gethostname() # Get local machine name
    port = 12345
    c = Client(host,port)
    #TEST1
    req = {'function':'coords'}
    print(c._exchange_messages(req))

    #TEST2
    #req = {'function':'set_origin'}
    #print(c._exchange_messages(req))

    # TEST3 Reinitialize tracking
    req = {'function':'reinitialise_tracking'}
    print(c._exchange_messages(req))