__author__ = 'madanh'
''' StereoCamera: This class uses two instances of Camera class to do it's dirty business'''


import cv2
from Camera import Camera
import numpy as np
import pickle

class StereoCamera(Camera):

    def __init__(self, name = 'StereoCamera',w = 1280,h = 720):
        self.name = name
        self.w = w
        self.h = h
        self.size = (w,h)
        self.cam = [Camera('left',0,w = self.w, h = self.h),Camera('right',1,w = self.w, h = self.h)] #initializa both cameras
        self.cams = (0,1)
        self.im = [None,None]


        # TODO: Set auto white balance ad autoexposure to false. Set WB to som same value for both cams

        # TRY to load existing calibration
        self.is_calibrated = self.load_intrinsics()

        # overlay text properties
        self.overlay_text = ['I AM A STEREO CAMERA!'] # a list of strings, must be a list even if has only one
        self.overlay_text_size = 1;
        self.overlay_text_color = (0,255,255);
        self.overlay_text_line_spacing = 30;



    def read(self):
        # grab and retrieve!
        if all([self.cam[i].grab() for i in self.cams]):
            for i in self.cams:
                _, self.im[i] = self.cam[i].retrieve()
            return self.im
        else:
            raise IOError('could not grab from both cameras')

    def show(self,window_name='preview',im=False,overlay_text = False, supress_pyrdown = False): #im is  alist of cv mats (nparrays)
        if im is False:
            im = self.im
        if self.w >=960 and not supress_pyrdown:
            im = [cv2.pyrDown(im[i].copy()) for i in (0,1)]
        to_display = np.concatenate(im,axis = 1)
        # if overlay_text

        cv2.imshow(window_name,to_display)

    def toggle_controls(self):

        for i in self.cams:
            self.cam[i].toggle_controls()

    def calibration_append(self):
        appending_success = [self.cam[i].calibration_append(self.im[i]) for i in self.cams] #boolean list
        if all(appending_success):
            return True #calibrate
        else:
            #cleanup
            #find indices  elements that are True and  remove_calibration_frame on corresp camera
            [self.cam[i].remove_calibration_frame() for i in self.cams if appending_success[i]]
        return False


    def calibration_append_and_show(self):
        if self.calibration_append():
                self.show('chessboards',[c.return_chessboard_corners_image() for c in self.cam])


    def calibrate_gui(self):
        #get individual cameras calibrated if not already
        for i in self.cams:
            if not self.cam[i].is_calibrated:
                self.cam[i].calibrate_gui()

        # CRITICAL: FLUSH the camera instances internal detected points!
        for c in self.cam:
            c.calibration_points=[]
            c.world_points=[]
            #note that we keep the Camera.corner_coordinates, as it will be appended internally


        #prepare for the loooooooop

        ch = 1
        while ch!=27:
            self.read()


            # preview
            self.show('preview',self.im)
            ch = cv2.waitKey(1) & 0xFF

            #switch-case dic here (python does not have normal switch-case)
            switch_case = {
                ord('a'):self.calibration_append_and_show,
                ord('c'):self.calibrate,
                ord('t'):self.toggle_controls,
            }

            # swith-case, see beginning of this method for cases
            func = switch_case.get(ch,lambda: "nothing")
            func()

            #show


        # I will use camera internal world_points structure
        # a stub, both camera images were synced and acquired the same board pattern and this patter was detected in both
    def calibrate(self):
        try:
            retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T, E, F = cv2.stereoCalibrate(np.array(self.cam[0].world_points),
                                np.array(self.cam[0].calibration_points),
                                np.array(self.cam[1].calibration_points),
                                self.cam[0].cameraMatrix,
                                self.cam[0].distCoeffs,
                                self.cam[1].cameraMatrix,
                                self.cam[1].distCoeffs,
                                self.size
                                )
            print retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T, E, F

            #on success change instance calibration params
            self.cam[0].cameraMatrix = cameraMatrix1
            self.cam[1].cameraMatrix = cameraMatrix2
            self.cam[0].distCoeffs = distCoeffs1
            self.cam[1].distCoeffs = distCoeffs2

            self.R = R
            self.T = T
            self.E = E
            self.F = F

            #### RECTIFY (I DEEM THIS SHIZZNIT AS A PART OF CALIBRATION)
            # TODO: add a corresponding methot to the StereoCamera class
            stereoRectify_output_tuple = cv2.stereoRectify(self.cam[0].cameraMatrix,
                          self.cam[0].distCoeffs,
						  self.cam[1].cameraMatrix,
                          self.cam[1].distCoeffs,
                          self.size,self.R,self.T
                          ) ## IMPORTANT: THIS IS THE CORRECT SIGNATURE AND WEB DOCS ARE LYING!!
                            #real signature:
                            #  """ stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, imageSize, R,
                            #  T[, R1[, R2[, P1[, P2[, Q[, flags[, alpha[, newImageSize]]]]]]]]) -> R1, R2, P1, P2, Q, validPixROI1, validPixROI2 """

            #unravel the tuple! With PATHOS!
            R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = stereoRectify_output_tuple

            self.cam[0].P = P1
            self.cam[1].P = P2

            self.cam[0].R = R1
            self.cam[1].R = R2

            self.cam[0].validPixROI = validPixROI1
            self.cam[1].validPixROI = validPixROI2

            for i in (0,1):
                self.cam[i].extrinsics_present = True

            self.Q = Q

            #### TRY TO SAVE RESULTS OF HARD WORK

            self.save_calibration()

            self.show_rectified_gui()










        except:
            import pdb
            pdb.set_trace()
            raise RuntimeError('Stereo Calibration did not succeed') #pops out when I accidatally press c instead of a


    def show_rectified_gui(self):
        #### TAKE A VISUAL INSPECTION

        map1 = [None,None]
        map2 = [None,None]
        self.undis = [None,None]


        self.ch = 0
        while self.ch != 27:
            self.read()
            for i in (0,1):
                map1[i],map2[i] = cv2.initUndistortRectifyMap(self.cam[i].cameraMatrix,self.cam[i].distCoeffs,self.cam[i].R,self.cam[i].P,self.size,cv2.CV_32FC1)
                self.undis[i] = cv2.remap(self.im[i],map1[i],map2[i],cv2.INTER_LINEAR)
                for line_y in range(0,self.h-1,20):
                    cv2.line(self.undis[i],(0,line_y),(self.w-1,line_y),(0,0,255),line_y%3+1)
            self.show(im = self.undis,supress_pyrdown = True)
            # cv2.imshow('undis %d' % i,self.undis[i])
            self.ch = cv2.waitKey(1)

        return #place for breakpoint

    def save_calibration(self):
        # pickle the stuff
        self.cam[0].save_intrinsics()
        self.cam[1].save_intrinsics()

        self.cam[0].save_extrinsics()
        self.cam[1].save_extrinsics()


        to_save = {
            'R':self.R,
            'T':self.T,
            'E':self.E,
            'F':self.F,
            'Q':self.Q,
                    }
        # pickle.dumps(to_save)
        with open('calibration_'+self.name+'.calibration','wb') as f:
            pickle.dump(to_save,f)


    def load_intrinsics(self, filename = False):
        if not filename:
            filename = 'calibration_'+self.name+'.calibration'
        try:
            with open(filename,'rb') as f:
                temp_dic = pickle.load(f)
                #print temp_dic
                self.R = temp_dic['R'];
                self.T = temp_dic['T'];
                self.E = temp_dic['E'];
                self.F = temp_dic['F'];

                self.cam[0].load_intrinsics()
                self.cam[1].load_intrinsics()

                self.cam[0].load_extrinsics()
                self.cam[1].load_extrinsics()

                if 'Q' in temp_dic:
                    self.Q = temp_dic['Q']
                else:
                    print 'StereoCamera: Loaded from old style pickled stereo calibration, Q matrix was not set\n'
                print "calibration parameters for camera " + self.name + ' were loaded successfully \n'
                return True
        except EnvironmentError:
            return False


    def rectify_show(self):
        if not self.is_calibrated:
            raise RuntimeError('cannot rectify, camera is not calibrated')
        self.read()
        who_nows_what = cv2.stereoRectify(self.cam[0].cameraMatrix,self.cam[0].distCoeffs,
                          self.cam[1].cameraMatrix,self.cam[1].distCoeffs,
                          self.cam[0].size,self.R,self.T
                          )

        print who_nows_what
        R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = who_nows_what

    def close(self):
        [self.cam[i].close() for i in self.cams]

    def test(self):
        # self.overlay_text = ['test']
        im_with_overlay = self.add_overlay_text(text = ['Im just a text'])
        cv2.imshow('preview',im_with_overlay)
        cv2.waitKey(15000)

    def add_overlay_text(self, im = False,text = False):
        """
        return a copy of input image with self.overlay_text overlayed
        text should be a list of strings
        :rtype : cv2.Mat == np.ndarray
        """
        if im is False:
            im = self.im

        if text is False:
            text = self.overlay_text
        the_image_to_pass_to_parent_class_method = np.concatenate(im,axis = 1)
        return super(StereoCamera,self).add_overlay_text(the_image_to_pass_to_parent_class_method,text = text) #yaaay reeel OOOP

    def undistort_points(self,point_coordinates_uv):

        return [self.cam[i].undistort_points(point_coordinates_uv[:,i,:]) for i in (0,1)]
        # TODO: IMPORTANT: DOUBLE CHECK THAT THE SLICING IS IN THE RIGHT DIRECTION

    def triangulate_points(self,undistorted_points):
        '''input argument undistorted_points is  a list of 2xn numpy arrays'''
        # returneth points in homo coords
        return cv2.triangulatePoints(self.cam[0].P,self.cam[1].P,undistorted_points[0],undistorted_points[1])


############################TEST CLAUSES #######################
if __name__ == '__main__':
    tests_to_run = [0,4]

    # TEST 0

    if 0 in tests_to_run:
        sc = StereoCamera()
        if not sc.is_calibrated:
            sc.calibrate_gui()
        sc.close()
    # TEST 1
    if 1 in tests_to_run:
        sc = StereoCamera()
        sc.rectify_show()
        sc.close()

    #TEST 2

    if 2 in tests_to_run:
        sc = StereoCamera()
        sc.calibrate_gui()
        sc.close()

    #TEST3
    if 3 in tests_to_run:
        sc = StereoCamera()
        sc.read()
        # sc.show()
        sc.test()
        sc.close()

    #TEST 4
    if 4 in tests_to_run:
        sc =StereoCamera()
        sc.show_rectified_gui()
        sc.close()

