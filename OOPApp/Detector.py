__author__ = 'Gena'

############# IMPORTANT TODOS #########################

# TODO: DUMB TRACKER - JUSTFIND MAXIUM BACKPROJECTION!!! THE MOST IMPORTANT THING
# TODO: MEDIAN PRE_FILTER FOR MEANSHIFTbb
# TODO: GAUSSIAN PREFILTER FOR HOUGH
# TODO: LIMIT FRAME EXPANSION BY HOUGH_RMAX
# TODO: BUG - Clear self.x,y on hough failure
# TODO: Add a tracker_initialized flag

import cv2
import numpy as np
from Camera import Camera  # TODO: should be optional, but we make bad code here
import pickle
from dprint import dprint



class TrackedBall(object):
    '''detectu chocoboru'''

    def __init__(self, im = None , cam = None, ROI = None, use_init_gui = False,hist_channels = [0,2], filename = None):
        #input "parsing"
        self.cam = cam
        if isinstance(self.cam,Camera):
            self.im = self.cam.read()
        else:
            self.cam = None

        if not hasattr(self, 'im'):
            if im is not None:
                self.im = im.copy()
            else:
                self.im = None

        # if input filename specified then unmarshall state from disk
        if filename is not None:
            self.unmarshall(filename)
            use_init_gui = False
        else:
                    # tracking data
            self.hist = None
            self.is_tracked = False
            self.x = None
            self.y = None
            self.r = None


        #!!! CHANNELS OF HSV TO BE USED FOR HISTO
        self.hist_channels = hist_channels

        # keycode of the last pressed char

        self.ch = 0

        # params
        self.canny_low = 60
        self.canny_hi = 100
        self.hough_rmin = 5
        self.hough_rmax = 25
        self.hough_accu = 45

        # overlay text properties
        self.overlay_text = ['I am a chocoboru tracker, konnichiwa'] # a list of strings, must be a list even if has only one
        self.overlay_text_size = 1;
        self.overlay_text_color = (0,255,255);
        self.overlay_text_line_spacing = 30;

        #flags
        self.flag_show_backprojection = False

        #containers
        self.prob = None # backprojected histogram = apriori probablity

        #constants
        self.controls_window_name = 'tracker controls'






        if not hasattr(self,'ROI'):
            self.ROI = ROI
        if self.ROI is None or use_init_gui:

            self.mode = 'init_gui'
            self.ROI_acquired = False
            #flags
            self.drag_start =None
            # _run the gui
            self.init_gui()

        # print self.ROI
        x0, y0, x1, y1 = self.ROI
        self.track_window = (x0, y0, x1-x0, y1-y0)





    def init_gui(self,window_name='individual cam tracker init'):
        '''lotsa spaghetti, dober tek'''
        # grab ROI


        cv2.namedWindow(window_name)
        cv2.setMouseCallback(window_name, self.onmouse)
        self.ch = 0
        while 1:
            if not self.ROI_acquired and self.ch !=27:
                self.overlay_text = ['Draw a rectangle around the BALLS!',
                     't:toggle controls',
                     'ESC:exit']
                if self.cam is not None:
                    self.im = self.cam.read()
                    with_text =  self.add_overlay_text()
                _ = self.overlay_ROI()

                if _ is False:
                    cv2.imshow(window_name,with_text)
                else:
                    cv2.imshow(window_name,_)
                    self.hough()
                    #cv2.imshow(window_name,_)


            else: # if ROI is acquired - track
                self.overlay_text = [
                    'b: toggle backprojection',
                    't: toggle controls',
                    'h: I am happy with tracking, please proceed',
                    'ESC:exit'
                ]
                # TODO: skip the following altogether if we have only a static image


                self.im = self.cam.read()
                self.track(self.im, flag_vis = True)
                if self.flag_show_backprojection:
                    _ = self.overlay_ROI(self.add_overlay_text(self.prob.copy()))
                else:
                    _ = self.overlay_ROI(self.add_overlay_text())

                if _ is False:
                    cv2.imshow(window_name,self.im)
                else:
                    cv2.imshow(window_name,_)

             # process input
            self.ch = cv2.waitKey(1) & 0xFF
            if self.ch == 27:
                break
            if self.ch == ord('t'):
                self.create_controls()
            if self.ch ==ord('b'):
                if self.prob is not None:
                    self.flag_show_backprojection = not self.flag_show_backprojection
                    cv2.imshow(window_name,self.prob)
            if self.ch == ord('h') and self.ROI_acquired:
                cv2.destroyWindow(self.controls_window_name)
                cv2.destroyWindow(window_name)
                break # track



    def overlay_ROI(self, im = None):
        if self.ROI is None:
            return False
        elif im is None :
            im = self.im.copy()

        cv2.rectangle(im,self.ROI[0:2],self.ROI[2:4],[255,0,0],thickness = 4)
        return im


    def onmouse(self, event, x, y, flags, param):
        if self.mode == 'init_gui':
            x, y = np.int16([x, y]) # BUG
        if event == cv2.EVENT_LBUTTONDOWN:
            self.drag_start = (x, y)
            self.ROI_acquired = 0
        if self.drag_start:
            if flags & cv2.EVENT_FLAG_LBUTTON:
                h, w = self.im.shape[:2]
                xo, yo = self.drag_start
                x0, y0 = np.maximum(0, np.minimum([xo, yo], [x, y]))
                x1, y1 = np.minimum([w, h], np.maximum([xo, yo], [x, y]))
                self.ROI = None
                if x1-x0 > 0 and y1-y0 > 0:
                    self.ROI = (x0, y0, x1, y1)
                    # self.crop() #it;s evening and too little time, sorry
            else:
                self.drag_start = None
                if self.ROI is not None:
                    self.ROI_acquired = True
                    self.norm_hist()


    # def start_tracking(self):
    #     # todo: sanity checks
    #     self.norm_hist()
    #     self.flag_tracking = True
    #
    # def stop


    def crop(self):
        x0, y0, x1, y1 = self.ROI
        self.cropped = self.im[y0:y1, x0:x1].copy()
        self.cropped = cv2.cvtColor(self.cropped,cv2.COLOR_BGR2HSV)
        return self.cropped.copy()

    def norm_hist(self):
        self.crop()
        self.mask() # debug
        cv2.imshow('cropped',self.cropped)
        if len(self.hist_channels) == 1:

            self.hist = cv2.calcHist([self.cropped],self.hist_channels,self.mask().astype(np.uint8),[16], [0, 255]) #cast to uint 8 , asrrgh

        else:
            self.hist = cv2.calcHist([self.cropped],self.hist_channels,self.mask().astype(np.uint8),[8,8], [0, 180,0,255]) #cast to uint 8 , asrrgh
        dprint('max hist = %s' % np.max(self.hist))
                # TODO: CHECK if I need astype np.float32 due to int overflow!!!
        cv2.normalize(self.hist, self.hist, 0, 255, cv2.NORM_MINMAX);
        dprint('max norm hist = %s' % np.max(self.hist))
        self.show_hist()


    def show_hist(self):

        if len(self.hist_channels) == 2:
            cv2.imshow('hist2d', self.hist)
        if len(self.hist_channels) == 1:
            bin_count = self.hist.shape[0]
            bin_w = 12
            img = np.zeros((256, bin_count*bin_w, 3), np.uint8)
            for i in xrange(bin_count):
                h = int(self.hist[i])
                cv2.rectangle(img, (i*bin_w+2, 255), ((i+1)*bin_w-2, 255-h), (int(180.0*i/bin_count), 255, 255), -1)
            img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)
            cv2.imshow('hist', img)

    def mask(self):
        x0, y0, x1, y1 = self.ROI
        w = y1-y0
        h = x1-x0
        self.masked = np.zeros((w,h)).astype(np.uint8);
        try:
            #if radius is not small decrease it, so that we have a cleaner histo
            if self.r is not None:
                r = self.r if self.r<9 else self.r-2
                r = int(r)
                cv2.circle(self.masked,(self.x,self.y),r,255,thickness = -1)
        except:
            import pdb
            pdb.set_trace()
            pass
        cv2.imshow('debug',cv2.bitwise_and(self.cropped,self.cropped,mask = self.masked))
        return self.masked


    def create_controls(self):
        cv2.namedWindow(self.controls_window_name)
        cv2.createTrackbar("Exposure",self.controls_window_name,int(-self.cam.cap.get(cv2.CAP_PROP_EXPOSURE)),11,lambda x: self.cam.cap.set(cv2.CAP_PROP_EXPOSURE,-x))
        cv2.createTrackbar("hough_rmin",self.controls_window_name,self.hough_rmin, 255, lambda x: setattr(self,'hough_rmin',x))
        cv2.createTrackbar("hough_rmax",self.controls_window_name,self.hough_rmax, 255, lambda x: setattr(self,'hough_rmax',x))
        cv2.createTrackbar("hough_accu",self.controls_window_name,self.hough_accu, 255, lambda x: setattr(self,'hough_accu',x))
        cv2.createTrackbar("canny_low",self.controls_window_name,self.canny_low, 255, lambda x: setattr(self,'canny_low',x))
        cv2.createTrackbar("canny_hi",self.controls_window_name,self.canny_hi, 255, lambda x: setattr(self,'canny_hi',x))


    def augment_ROI(self):
        ########## AUGMENT THE ROI!!
        x0, y0, x1, y1 = self.ROI
        delta = 0 # extra margin in px
        self.ROI = (x0+int(self.x-self.r)-delta,y0+int(self.y-self.r)-delta,x0+int(self.x+self.r)+delta,y0+int(self.y+self.r)+delta)

    def hough(self, flag_vis = False):

        x0, y0, x1, y1 = self.ROI
        self.crop()
        vis_roi = self.cropped
               ######### CHT HOUGH #################
        # get 1 cannel im
        # gray_roi = cv2.cvtColor(vis_roi,cv2.COLOR_BGR2GRAY)
        gray_roi = cv2.cvtColor(vis_roi,cv2.COLOR_BGR2HSV)
        gray_roi = gray_roi[:,:,2] #get only value
        # get a copy to show the circle overlay
        cimg = vis_roi.copy() # numpy function
        if flag_vis:
            cv2.imshow("source", vis_roi)
            cv2.imshow("Canny",cv2.Canny(gray_roi,self.canny_low, self.canny_hi))
        circles = cv2.HoughCircles(gray_roi, cv2.HOUGH_GRADIENT, 2, 10, np.array([]), self.canny_hi, self.hough_accu,self.hough_rmin,self.hough_rmax)#, 5, 100)

        if circles is not None:
            if (circles.shape[1]==1): #got exactly one circle
                self.x = circles[0,0,0]
                self.y = circles[0,0,1]
                self.r = circles[0,0,2]

                # TODO: calculate the bounding box here
                if flag_vis:
                    i = 0 #i was too lazy to replace all i's, sorry
                    cv2.circle(cimg, (circles[0][i][0], circles[0][i][1]), circles[0][i][2], (0, 0, 255), 3, cv2.LINE_AA)
                    cv2.circle(cimg, (circles[0][i][0], circles[0][i][1]), 2, (0, 255, 0), 3, cv2.LINE_AA) # draw center of circle
                    cv2.imshow("detected circles", cimg)


                return True

            else:# got multiple circles
                return False
        else:
            return False

        # if circles is not None:
        #     a, b, c = circles.shape
        #     for i in range(b):
        #         cv2.circle(cimg, (circles[0][i][0], circles[0][i][1]), circles[0][i][2], (0, 0, 255), 3, cv2.LINE_AA)
        #         cv2.circle(cimg, (circles[0][i][0], circles[0][i][1]), 2, (0, 255, 0), 3, cv2.LINE_AA) # draw center of circle
        #         #cv2.Canny()



        return circles
        ###########END CHT ##################


    def track(self,new_im, flag_vis = False, copy = True):
        if copy:
            self.im = new_im.copy()
        else:
            self.im = new_im
        x0, y0, x1, y1 = self.ROI
        self.track_window = (x0, y0, x1-x0, y1-y0)
        hsv = cv2.cvtColor(new_im,cv2.COLOR_BGR2HSV)
        cv2.imshow('hsv',hsv[:,:,0])
        mask = cv2.inRange(hsv, np.array((0., 60., 60.)), np.array((180., 255.,255.))) #don't consider too dark or bright pixels
        cv2.imshow('inrange mask',mask)
        # TODO: augment the line above
        # this sometimes crashes, so ill try to find out useing this try catch
        try:
            if len(self.hist_channels)==1:
                self.prob = cv2.calcBackProject([new_im],self.hist_channels,self.hist, [0, 180],1)
            else:
                self.prob = cv2.calcBackProject([new_im],self.hist_channels,self.hist, [0, 180,0,255],1)
        except:
            debuging_trigger = True
            import pdb
            pdb.set_trace()
            pass #Place a conditional breakpoint on this line

        try:
            self.prob &= mask
        except:
            debuging_trigger = True
            import pdb
            pdb.set_trace()
            pass #Place a conditional breakpoint on this line
        term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )

        # track_box, self.track_window = cv2.meanShift(self.prob, self.track_window, term_crit)
        track_box, self.track_window = cv2.meanShift(hsv[:,:,2], self.track_window, term_crit)

        x0,y0,dx,dy = self.track_window
        x1 = x0+dx
        y1 = y0+dy
        self.ROI = (x0,y0,x1,y1)
        return x0+dx/2, y0+dy/2
        if self.hough(flag_vis = flag_vis):
            return x0 + self.x , y0 + self.y
            # self.augment_ROI()
            pass
        else:
            return x0+dx/2, y0+dy/2
            # self.expand_ROI()
            pass


    def expand_ROI(self, d =1): # d = how many pixels to expand
        x0, y0, x1, y1 = self.ROI

        h = self.im.shape[0]
        w = self.im.shape[1]
        x0 = 0 if x0 - d <=0 else x0 - d
        y0 = 0 if y0 - d <=0 else y0 - d
        x1 = w if x1+d>w else x1+d
        y1 = h if y1+d>h else y1+d

        self.ROI = x0,y0,x1,y1


    def add_overlay_text(self, im = None,text = False):
        if im is None:
            im = self.im.copy() #if second argument was not specified
        if text is False:
            text = self.overlay_text
        nlines = len(text); #how many line to output
        for i in range(nlines):
            cv2.putText(im,text[i],(0,im.shape[0]-(nlines-i-1)*self.overlay_text_line_spacing-self.overlay_text_line_spacing/2),
                        cv2.FONT_HERSHEY_COMPLEX,self.overlay_text_size,self.overlay_text_color)

        return im


    def save_hist(self,filename):
        # to_save = {'cameraMatrix':self.cameraMatrix,
        #            'distCoeffs':self.distCoeffs,
        #            }
        # pickle.dumps(to_save)
        with open(filename,'wb') as f:
            pickle.dump(self.hist,f)


    def load_hist(self,filename):
        with open(filename,'rb') as f:
            self.hist = pickle.load(f)


    def marshall(self, filename  = None):
        if filename is None:
            import pdb
            pdb.set_trace() #breakpoint so that we don't loose all because I forgot to specify the filename


        to_save_string_list = ['hist','hist_channels','ROI','hough_accu','hough_rmax','hough_rmin','canny_hi','canny_low','im']
        # build a dict to save
        to_save = {}
        for key in to_save_string_list:
            to_save[key] = self.__getattribute__(key)

        # save
        try:
            with open(filename,'wb') as f:
                pickle.dump(to_save,f)
        except EnvironmentError:
            raise IOError('BallTracker:Could not save to file')
        print 'seems like we have saved tracker settings'


    def unmarshall(self,filename):
        if filename is None:
            import pdb
            pdb.set_trace() #breakpoint so that we don't loose all because I forgot to specify the filename
        try:
            with open(filename,'rb') as f:
                to_load = pickle.load(f)
        except EnvironmentError:
            raise IOError('TrackedBall: cannot unmarshal due to file reading error')
        except:
            import pdb
            pdb.set_trace()

        for key in to_load.keys():
            self.__setattr__(key,to_load[key])

        print 'seems like we have loaded tracker settings'




############TESETS! ################
if __name__ == '__main__':
    tests_to_run = [0]

    if 0 in tests_to_run:
        from Camera import Camera
        cam = Camera('Salty cam', device_number= 0)
        tb = TrackedBall(cam = cam,hist_channels=[0,1])

        tb.marshall('test.tracker.state')

        tb_ressurected = TrackedBall(filename = 'test.tracker.state')
        tb_ressurected.track(tb.im)