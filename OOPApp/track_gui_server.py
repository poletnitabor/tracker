__author__ = 'Gena'



import socket               # Import socket module
import matplotlib
# print matplotlib.matplotlib_fname()
matplotlib.use('Qt4Agg') #DOING IT HERE SINCE IN THE MAIN CODE IT WOULD LOOK EXTRANEOUS
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import numpy as np


if __name__ =='__main__':
    # INIT SERVER
    s = socket.socket()         # Create a socket object
    host = socket.gethostname() # Get local machine name
    port = 12345                # Reserve a port for your service.
    s.bind((host, port))        # Bind to the port

    s.listen(5)                 # Now wait for client connection.

    # INIT GUI
    #     #visualiozation init
    # plt.ion()
    # # plt.close('all')
    # fig = plt.figure()
    # ax = fig.add_subplot(111,projection='3d')
    #
    #
    #
    #
    # cm = ('r','g','b')#colormap
    # ch = 0
    #
    # ax.hold(True)
    # ax.set_zlim(0,150)
    # ax.set_xlim(-30,30)
    # ax.set_ylim(-30,30)
    #
    #
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')

    # MAIN LOOP
    while True:
       c, addr = s.accept()     # Establish connection with client. This is blocking!
       print 'Got connection from', addr
       while True:
           try:
               instring = c.recv(128)
               print instring

               #PARSE THE RECIEVED STUFF
               for line in instring.splitlines():
                   while len(line):
                       coords, instring = line.split(',',3) #consume 3 elements of the instring to be used as coords

                       coords = [float(coords[_]) for _ in range(3)]

                       #d = sqrt(coords[1]**2+coords[2]**2+coords[3]**2)
                       print coords


           except:
               print('could not recv(), assuming connecion close')
               break

    # NEVER HAPPENS:
    c.send('Thank you for connecting')
    c.close()                # Close the connection