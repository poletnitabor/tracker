__author__ = 'Gena'

# TODO: PREVIEW WINDOW TO SEE WHEN TRACKING FAILS
from StereoCamera import StereoCamera
from Detector import TrackedBall
import cv2
import numpy as np

import dprint #local package for diagnostic prints


class StereoTracker(object):

    ''' stereotracker is a stereocam and two trackers (:'''


    def __init__(self, name = 'unnamed tracker', n = 6,use_init_gui = False): # how many objects to track

        self.name = name
        self.n = n
        self.sc = StereoCamera()
        if not self.sc.is_calibrated:
            self.sc.calibrate_gui()
        self.im = []
        self.overlay_text = ['StereoTracker did not initialize']
        self.tracker = [(None,None) for i in range(self.n)]
        self.homo = None
        self.xyz = None

        self.ch = 0 #keycode


    # def tracker_gui(self, online_mode = False):
    #     # have operators put the tracking deviceinto the FOV and capture
    #     # if not self._tracker_gui_window_1():
    #     #     raise RuntimeError('tracker init aborted')
    #     # init individual trackers
        if not use_init_gui:
            try: #try to load state
                self.tracker = [tuple(TrackedBall(filename = 'tracker_%d_%d.state' % (j,i))  for i in (0,1)) for j in range(self.n)]
                return # escape from the function on success
            except IOError: # do usual init on file ops failure
                pass
        # IMPORTANT: CHANNELS BELOW
        self.tracker = [tuple(TrackedBall(cam = self.sc.cam[i],hist_channels=[0]) for i in (0,1)) for j in range(self.n)]
        # Trying to save
        try:
            self.save_state()
        except EnvironmentError:
            print('StereoTracker warning: could not save tracking state due to IO failure\n')

        self.update() # populate self.im

    def save_state(self):
        try:
            [[self.tracker[j][i].marshall('tracker_%d_%d.state' % (j,i))  for i in (0,1)] for j in range(self.n)]
            return True
        except EnvironmentError:
            print('StereoTracker warning: could not save tracking state due to IO failure\n')
            return False




    def _tracker_gui_window_1(self):
        '''
        :return:first phase of stereo trackr init
        '''

        winname = 'Tracker initialization'
        self.overlay_text = ['Put the tracking device so that',
                             'it is visible to both cameras',
                             'SPACE: capture an image and proceed',
                             't: toggle camera controls',
                             'ESC: abort tracker initialization'
                             ]
        ch = 0
        while ch != 32:
            self.im = self.sc.read()
            long_image = self.sc.add_overlay_text(self.im,self.overlay_text)
            cv2.imshow(winname,long_image)
            ch = cv2.waitKey(1) & 0xFF
            if ch == ord('t'):
                self.sc.toggle_controls()
            if ch == 27: #if escape pressed
                cv2.destroyWindow(winname)
                return False
        # capture the image and ask the guys to select markers one by one
        cv2.destroyWindow(winname)
        return True

    def track(self, diagnostic_visual = True):
        # TODO: allow for reuse of a frame from different tracker don't capture all the time (or refactor the class to include multiple TrackedBalls objects
        # locate
        self.coords_intrinsic = [tuple(self.tracker[j][i].track(self.im[i]) for i in (0,1)) for j in range(self.n)]
        self.coords_intrinsic = np.array(self.coords_intrinsic)
        #### undistort
        # import pdb
        # pdb.set_trace()
        # dprint.dprint(self.coords_intrinsic)
        # dprint.dprint('\n')
        self.undistorted = self.sc.undistort_points(self.coords_intrinsic)
        # TODO: IMPORTANT: DOUBLE CHECK THAT THE SLICING IS IN THE RIGHT DIRECTION (INSIDE THE FUNCTION
        if diagnostic_visual:
            # dprint.dprint(self.undistorted)
            pass





        self.homo = self.sc.triangulate_points(self.undistorted)
        self.xyz = np.divide(self.homo,self.homo[3])[:3,:]
        # self.xyz[]

        if diagnostic_visual:
            self.diagnostic_visual()
        return self.xyz


    def diagnostic_visual(self,window_name = None, downsample = True):
        if window_name is None:
            window_name = 'diag vis for stereo tracker' + self.name
        # draw xy on the corresponding image
        im = [cv2.pyrDown(self.im[i].copy()) for i in (0,1)]
        colormap = [(0,0,255),(0,255,0),(255,0,0),(255,255,0),(255,0,255),(255,255,255)]

        for cam_index in (0,1):
            for point_index in range(self.coords_intrinsic.shape[0]):
                integer_uv = tuple(int(self.coords_intrinsic[point_index,cam_index,coordinate_index]/2) for coordinate_index in (0,1))
                # integer_coords = tuple( tuple( int(element/2) for element in subtuple) for subtuple in self.coords_intrinsic)
                _  = [cv2.circle(im[cam_index],integer_uv,5,colormap[point_index],2) for i in (0,1)]
            _ = cv2.imshow(window_name+' '+ str(cam_index),im[cam_index])


        self.ch = cv2.waitKey(1)
        if self.ch == ord('u'):
            self.sc.show_rectified_gui()
        return self.ch


    def update(self):
        self.im = self.sc.read() #read one frame from stereo camera
        self.ch = cv2.waitKey(1)

    def close(self):
        self.sc.close()




#####   # TEST 0.
def test0():
    st = StereoTracker(n = 2)
    for i in range(10):
        st.update()
        st.track(diagnostic_visual=True)



    import matplotlib
    # print matplotlib.matplotlib_fname()
    # matplotlib.use('Qt4Agg')

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import axes3d
    import numpy as np


    #visualiozation init
    plt.ion()
    # plt.close('all')
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')




    cm = ('r','g','b')#colormap
    ch = 0

    ax.hold(True)
    ax.set_zlim(0,150)
    ax.set_xlim(-30,30)
    ax.set_ylim(-30,30)


    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    while st.ch != 27:
        st.update()

        # for i in range(1):

        # xyz[i].append(st.track(diagnostic_visual=True))




        # ax.hold(False)
        ####### PRIMARY TEST

        xyz = st.track(diagnostic_visual=True)
        ax.plot(xyz[0,:],xyz[1,:],xyz[2,:])
        # print (xyz[0,:],xyz[1,:],xyz[2,:])
        ######### SECONDARY TEST
        # ax.plot(xyz[i][0],xyz[i][1],xyz[i][2], c=cm[i])#, marker='o')


        # ax.hold(True)

        #### UNCOMENT FOR PDB BREAKPOINT
        # import pdb
        # pdb.set_trace()
        # ax.text(xyz[i][0],xyz[i][1],xyz[i][2],'%.0f, %.0f, %.0f' % tuple(xyz[0:3]))



        plt.pause(0.0001)
        #print xyz


            # plt.show(block = False)
if __name__ == '__main__':
    test_to_run = [0]

    # TEST 0.
    if 0 in test_to_run:
        test0()

    # ####### START THE CLIENT
    #
    # import socket               # Import socket module
    #
    # s = socket.socket()         # Create a socket object
    # host = socket.gethostname() # Get local machine name
    # port = 12345                # Reserve a port for your service.
    #
    # s.connect((host, port))
    #
    # ####### INIT THE TRACKER
    #
    # st = StereoTracker(n = 2)
    #
    #
    # ###### MAIN LOOP (SERVE)
    # while st.ch != 27:
    #     st.update()
    #     xyz = st.track(diagnostic_visual=True)
    #     # parse tracker output
    #     xyz[:,0]
    #     outstring =''
    #     for i in range(st.n):
    #         outstring += ','.join(['%.1f' % element for element in xyz[:,i]])
    #         outstring += ','
    #     outstring+='\n'
    #
    #     #send through socket
    #     s.sendall(outstring)
    #
    # s.close()                     # Close the socket when done


