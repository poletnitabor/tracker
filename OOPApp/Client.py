__author__ = 'Gena'

import socket
import pickle
import numpy as np

import matplotlib
# print matplotlib.matplotlib_fname()
matplotlib.use('Qt4Agg') #DOING IT HERE SINCE IN THE MAIN CODE IT WOULD LOOK EXTRANEOUS
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d


class Client(object):

    def __init__(self,host = 'localhost', port = 12345):
        self._address = host
        self._port = port
        self.x_pressed = False
    #--------------------------------------------------------------------------
    # SOCKET COMMUNICATION PROTOCOL #Stolen and adapted ^w^w^w Backported from Ziga
    #--------------------------------------------------------------------------
    def _decode_message(self, data_bytes):
        return pickle.loads(data_bytes)


    def _encode_message(self, data_dict):
        return pickle.dumps(data_dict,2)


    def _exchange_messages( self, message, address='localhost', port=11000 ):
        """Exchange message-response over socket interface"""
        # create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # connect the socket to the port where the server is listening
        server_address = (self._address, self._port)
        sock.connect(server_address)
        # set default response
        response = {}
        try:
            # send data
#            dprint("message: %s" % message)
#            dprint("self._encode_message(message): %s " % self._encode_message(message))
            sock.sendall( self._encode_message(message) )
#            dprint("Start receiving...")
            # look for the response
            data_bytes = sock.recv(1024)
            # decode message
#            dprint( "data_bytes: %s" % data_bytes )
            response = self._decode_message( data_bytes )
#            dprint( "response: %s" % response )
        finally:
            # close socket
            sock.close()

            # return response
            return response

        return bytes(data_bytes, encoding)

    def coords(self):
        req = {'function':'coords'}
        return self._exchange_messages(req)

    def RT(self):
        req = {'function':'RT'}
        return self._exchange_messages(req)

    def _check_angles(self):
        self.message = self.coords()
        self.c = self.message['message']
        self.i = self.c[1,:]-self.c[0,:]
        self.j = self.c[2,:]-self.c[0,:]
        dot = np.dot(self.i,self.j)
        cos = dot/(np.linalg.norm(self.i)*np.linalg.norm(self.j))
        angle = np.degrees(np.arccos(cos))
        return angle

    def _check_distances(self):
        self.message = self.coords()
        self.c = self.message['message']
        self.i = self.c[:,1]-self.c[:,0]
        self.j = self.c[:,2]-self.c[:,0]
        self.hipo = self.c[:,2]-self.c[:,1]
        a= np.linalg.norm(self.i)
        b= np.linalg.norm(self.j)
        c= np.linalg.norm(self.hipo)

        return a,b,c

    def _check_RT(self):
        self.message = self.RT()
        if self.message['error']=='0':
            self.R = self.message['R']
            self.T = self.message['T']
            print self.R
            print '\n'
            print self.T
        else:
            print 'RT failed'


    def press(self,event): # input handler
        if event.key == 'x':
            self.x_pressed = True
            return True
        else:
            self.x_pressed = False
            return False

    def set_origin(self):
        req = {'function':'set_origin'}
        print self._exchange_messages(req)
        self.x_pressed= False

    def reinitialise_tracking(self):
        req = {'function':'reinitialise_tracking'}
        print self._exchange_messages(req)

    def save_state(self):
        req = {'function':'save_state'}
        print self._exchange_messages(req)

    def close(self):
        req = {'function':'close'}
        print self._exchange_messages(req)

    def shutdown(self):
        req = {'function':'shutdown'}
        print self._exchange_messages(req)




if __name__=='__main__':
    tests_to_run = ['vis']

    host = socket.gethostname() # Get local machine name
    port = 12345
    c = Client(host,port)
    #TEST1
    if 1 in tests_to_run:
        req = {'function':'coords'}
        print c._exchange_messages(req)

    #TEST2
    if 2 in tests_to_run:
        req = {'function':'set_origin'}
        print c._exchange_messages(req)

    #TEST3
    if 3 in tests_to_run:
        req = {'function':'RT'}
        print c._exchange_messages(req)

    #TEST 4 angle conservation
    if 4 in tests_to_run:
        while 1:
            print c._check_angles()

    #TEST 5 distance conservation
    if 5 in tests_to_run:
        while 1:
            print c._check_distances()

    #TEST 6 distance conservation
    if 6 in tests_to_run:
        req = {'function':'set_origin'}
        print c._exchange_messages(req)
        while 1:
            c._check_RT()

    # visualization
    if 'vis' in tests_to_run:

        #visualiozation init
        plt.ion()
        # plt.close('all')
        fig = plt.figure()
        fig.canvas.mpl_connect('key_press_event', c.press)
        ax = fig.add_subplot(111,projection='3d')




        cm = ('r','g','b')#colormap
        ch = 0

        ax.hold(False)
        ax.set_zlim(-10,50)
        ax.set_xlim(-50,50)
        ax.set_ylim(-50,50)


        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        while True:
            ax.hold(False)
            plt.pause(0.0001)
            for dummy_i in range(20):

                mes = c.coords()
                xyz = mes['message']
                # print 'xyz %s' % xyz


                ax.scatter(list(xyz[:,0]),list(xyz[:,1]),list(xyz[:,2]),c = 'r', marker = 'o')
                ax.scatter(list(xyz[1:2,0]),list(xyz[1:2,1]),list(xyz[1:2,2]),c = 'b', marker = 'o')

                ax.set_zlim(-10,50)
                ax.set_xlim(-50,50)
                ax.set_ylim(-50,50)


                ax.set_xlabel('x')
                ax.set_ylabel('y')
                ax.set_zlabel('z')
                ax.hold(True)
                plt.pause(0.0001)
                # print mes



                if c.x_pressed:
                    c.set_origin()

                # CHECK RT
                c._check_RT()




                #check angles

                print c._check_angles()
                print c._check_distances()




