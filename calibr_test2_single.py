__author__ = 'Gena'

import cv2
import time
from stereovision import calibration

######################## INIT #########################

ncams = 1

cap = [cv2.VideoCapture(x) for x in range(ncams)]
im = range(ncams) #init the list for later indexing
ret = im
# some spaghetization :(
calibrator = calibration.StereoCalibrator(rows= 6, columns = 9,square_size = 2.54, image_size = (640, 480))


while 1:
    for i in range(ncams):
        #quickly grab a frame fro each camera
        ret[i] = cap[i].grab()


    for i in range(ncams):
        # retrieve the grabbed images
        if ret[i]:
            ret[i], im[i] = cap[i].retrieve()
        else:
            raise IOError('could not grab image from camera')

        cv2.imshow(str(i),im[i])
    if cv2.waitKey(1) == ord('q'):
        break
    try:
        calibrator.add_corners((im[0],im[1]),show_results=True)
        # temp = image
        # cv2.drawChessboardCorners(temp, (self.rows, self.columns), corners,
        #                           True)
        # window_name = "Chessboard"
        # cv2.imshow(window_name, temp)
        time.sleep(1)
    except calibration.ChessboardNotFoundError:
        print 'no chessboard pattern present'


calibration_object = calibrator.calibrate_cameras()


print ' I PRINTAH!'
