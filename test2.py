import cv2
import numpy as np

cap = cv2.VideoCapture(0)

#set low exposure

cap.set(cv2.CAP_PROP_EXPOSURE,-7.0)

#pin white balance
cap.get(cv2.CAP_PROP_WHITE_BALANCE_BLUE_U)

while(1):

    # Take each frame
    _, frame = cap.read()
    '''
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)


    # define range of pink color in HSV
    lower_pink = np.array([0/2,200,63])
    upper_pink = np.array([25/2,255,255])

    # Threshold the HSV image to get only blue colors
    mask_pink = cv2.inRange(hsv, lower_pink, upper_pink)

    #define range of blue color in HSV
    lower_green = np.array([60/2,31,191])
    upper_green = np.array([160/2,255,255])

    # Threshold the HSV image to get only blue colors
    mask_green = cv2.inRange(hsv, lower_green, upper_green)




    #combine green and pink
    mask = mask_pink+mask_green
    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask = mask )

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('res',res)
    '''

    img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,
                            param1=50,param2=30,minRadius=0,maxRadius=0)
    print circles


    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()