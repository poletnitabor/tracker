#!/usr/bin/env python

'''
Camshift tracker
================

This is a demo that shows mean-shift based tracking
You select a color objects such as your face and it tracks it.
This reads from video camera (0 by default, or the camera number the user enters)

http://www.robinhewitt.com/research/track/camshift.html

Usage:
------
    camshift.py [<video source>]

    To initialize tracking, select the object with mouse

Keys:
-----
    ESC   - exit
    b     - toggle back-projected probability visualization
    c     - show control sliders for parameter tuning
'''

import numpy as np
import cv2

# local module
#import video   #commented this out as unnecessary

class App(object):
    def __init__(self, video_src):
        #names
        self.controls_window_name = "TEH LEVERS!"
        self.cam = cv2.VideoCapture(video_src);
        ret, self.frame = self.cam.read()
        cv2.namedWindow('camshift')
        cv2.setMouseCallback('camshift', self.onmouse)

        #flags
        self.selection = None
        self.drag_start = None
        self.tracking_state = 0
        self.show_backproj = False
        self.show_controls =True

        #params
        self.canny_low = 60
        self.canny_hi = 100
        self.hough_rmin = 5
        self.hough_rmax = 20
        self.hough_accu = 60

    def dummy(self,_):
        pass

    def onmouse(self, event, x, y, flags, param):
        x, y = np.int16([x, y]) # BUG
        if event == cv2.EVENT_LBUTTONDOWN:
            self.drag_start = (x, y)
            self.tracking_state = 0
        if self.drag_start:
            if flags & cv2.EVENT_FLAG_LBUTTON:
                h, w = self.frame.shape[:2]
                xo, yo = self.drag_start
                x0, y0 = np.maximum(0, np.minimum([xo, yo], [x, y]))
                x1, y1 = np.minimum([w, h], np.maximum([xo, yo], [x, y]))
                self.selection = None
                if x1-x0 > 0 and y1-y0 > 0:
                    self.selection = (x0, y0, x1, y1)
            else:
                self.drag_start = None
                if self.selection is not None:
                    self.tracking_state = 1


    def show_hist(self):
        pass
        # bin_count = self.hist.shape[0]
        # bin_w = 12
        # img = np.zeros((256, bin_count*bin_w, 3), np.uint8)
        # for i in xrange(bin_count):
        #     h = int(self.hist[i])
        #     cv2.rectangle(img, (i*bin_w+2, 255), ((i+1)*bin_w-2, 255-h), (int(180.0*i/bin_count), 255, 255), -1)
        # img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)
        # cv2.imshow('hist', img)


    # def on_exposure_change(self,value):
    #     # set exposure
    #     self.cam.set(cv2.CAP_PROP_EXPOSURE,-value)

    def create_controls(self):
        cv2.namedWindow(self.controls_window_name)
        cv2.createTrackbar("Exposure",self.controls_window_name,int(-self.cam.get(cv2.CAP_PROP_EXPOSURE)),11,lambda x: self.cam.set(cv2.CAP_PROP_EXPOSURE,-x))
        cv2.createTrackbar("hough_rmin",self.controls_window_name,self.hough_rmin, 255, lambda x: setattr(self,'hough_rmin',x))
        cv2.createTrackbar("hough_rmax",self.controls_window_name,self.hough_rmax, 255, lambda x: setattr(self,'hough_rmax',x))
        cv2.createTrackbar("hough_accu",self.controls_window_name,self.hough_accu, 255, lambda x: setattr(self,'hough_accu',x))
        cv2.createTrackbar("canny_low",self.controls_window_name,self.canny_low, 255, lambda x: setattr(self,'canny_low',x))
        cv2.createTrackbar("canny_hi",self.controls_window_name,self.canny_hi, 255, lambda x: setattr(self,'canny_hi',x))

    # def poll_controls(self):
    #     # set exposure
    #     self.cam.set(cv2.CAP_PROP_EXPOSURE,-cv2.getTrackbarPos("Exposure",self.controls_window_name))

    def run(self):
        self.create_controls();
        while True:
            ret, self.frame = self.cam.read()
            self.frame=cv2.medianBlur(self.frame,3)
            vis = self.frame.copy()
            hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
            mask = cv2.inRange(hsv, np.array((0., 60., 32.)), np.array((180., 255., 255.)))

            if self.selection:
                x0, y0, x1, y1 = self.selection
                self.track_window = (x0, y0, x1-x0, y1-y0)
                hsv_roi = hsv[y0:y1, x0:x1]
                mask_roi = mask[y0:y1, x0:x1]
                hist = cv2.calcHist( [hsv_roi], [0,1], mask_roi, [10,10],[0, 180,0,255] )
                cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX);
                self.hist = hist
                self.show_hist()

                vis_roi = vis[y0:y1, x0:x1]
                ######### CHT HOUGH #################
                # get 1 cannel im
                gray_roi = cv2.cvtColor(vis_roi,cv2.COLOR_BGR2GRAY)

                # get a copy to show the circle overlay
                cimg = vis_roi.copy() # numpy function
                cv2.imshow("source", vis_roi)
                cv2.imshow("Canny",cv2.Canny(gray_roi,self.canny_low, self.canny_hi))
                circles = cv2.HoughCircles(gray_roi, cv2.HOUGH_GRADIENT, 2, 10, np.array([]), self.canny_hi, self.hough_accu,self.hough_rmin,self.hough_rmax)#, 5, 100)
                print self.hough_rmin
                if circles is not None:
                    a, b, c = circles.shape
                    for i in range(b):
                        cv2.circle(cimg, (circles[0][i][0], circles[0][i][1]), circles[0][i][2], (0, 0, 255), 3, cv2.LINE_AA)
                        cv2.circle(cimg, (circles[0][i][0], circles[0][i][1]), 2, (0, 255, 0), 3, cv2.LINE_AA) # draw center of circle
                        #cv2.Canny()


                    cv2.imshow("detected circles", cimg)

                ###########END CHT ##################
                cv2.bitwise_not(vis_roi, vis_roi)
                vis[mask == 0] = 0

            if self.tracking_state == 1:
                cv2.imshow("Canny",cv2.Canny(gray_roi,self.canny_low, self.canny_hi))
                self.selection = None
                prob = cv2.calcBackProject([hsv], [0,1], self.hist, [0, 180,0,255], 1)
                prob &= mask
                term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )
                track_box, self.track_window = cv2.meanShift(prob, self.track_window, term_crit)

                if self.show_backproj:
                    vis[:] = prob[...,np.newaxis]
                try:
                    #cv2.ellipse(vis, track_box, (0, 0, 255), 2)

                    # Draw it on image
                    x,y,w,h = self.track_window
                    img2 = cv2.rectangle(vis, (x,y), (x+w,y+h), 255,2)
                    cv2.imshow('img2',img2)
                except:

                    print track_box


            cv2.imshow('camshift', vis)
            ################## CONTROLS #######################
            # if self.show_controls:
            #     self.poll_controls()

            #################### END CONTROLS #################
            ch = 0xFF & cv2.waitKey(5)
            if ch == 27:
                break
            if ch == ord('b'):
                self.show_backproj = not self.show_backproj
            # if ch == ord('c'):
            #     self.show_controls = not self.show_controls
        cv2.destroyAllWindows()


if __name__ == '__main__':
    import sys
    try:
        video_src = sys.argv[1]
    except:
        video_src = 1
    print __doc__
    App(video_src).run()
