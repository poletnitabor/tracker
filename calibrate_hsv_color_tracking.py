__author__ = 'Gena'

# A module to get HSV limits of objects you want to track
# Run this before tracking session or on lightning change
# User manually adjusts the HSV limits to get a good segmentation of his object of interest and hits the button to use
# Those values

import cv2

# a dummy function to be used as a callback for the trackbars from opencv
def dummy(x):
    pass

# Draw the UI controls

def draw_ui_controls(window_name):
    cv2.namedWindow(window_name)
    cv2.createTrackbar("H_min",window_name,0,255,dummy)
    cv2.createTrackbar("H_max",window_name,0,255,dummy)
    cv2.createTrackbar("S_min",window_name,0,255,dummy)
    cv2.createTrackbar("S_max",window_name,0,255,dummy)
    cv2.createTrackbar("V_min",window_name,0,255,dummy)
    cv2.createTrackbar("V_max",window_name,0,255,dummy)
    cv2.createTrackbar("Exposure",window_name,0,11,dummy)

def poll_trackbars(window_name):
    out = {}
    out["H_min"]=cv2.getTrackbarPos("H_min",window_name)
    out["H_max"]=cv2.getTrackbarPos("H_max",window_name)
    out["S_min"]=cv2.getTrackbarPos("S_min",window_name)
    out["S_max"]=cv2.getTrackbarPos("S_max",window_name)
    out["V_min"]=cv2.getTrackbarPos("V_min",window_name)
    out["V_max"]=cv2.getTrackbarPos("V_max",window_name)
    out["Exposure"]=cv2.getTrackbarPos("Exposure",window_name)

    lower = out["H_min"], out["S_min"], out["V_min"]
    upper = out["H_max"], out["S_max"], out["V_max"]

    return lower, upper, out["Exposure"]


# test function
def main_test():

    try: #wrap all up in exception handler in order to properly close he camera sream

        #raise a window with controls
        window_name  = 'Press enter to set values, ESC to exit'
        draw_ui_controls(window_name)

        # get a capture object
        cap = cv2.VideoCapture()
        #connect to camera 1
        cap.open(0) #not testing for success :(

        while(1):
            # get frame
            status, frame = cap.read()
            # Convert BGR to HSV
            hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            #poll the trackbars
            lower, upper, exposure =  poll_trackbars(window_name)
            #adjust exposure
            cap.set(cv2.CAP_PROP_EXPOSURE,-exposure)
            #"segment" the colors
            mask = cv2.inRange(hsv, lower, upper)
            #show original
            cv2.imshow('frame',frame)
            #show _segmented
            res = cv2.bitwise_and(frame,frame, mask = mask )
            cv2.imshow('segmentish',res)
            #check if enter is pressed
            if cv2.waitKey(1) in [13,27]: # on enter or escape
                break
    except:
        print 'Something wen wrong (exception caught'
    finally: #whether succeeded or not, close the camera stream
        cap.release()



# if _run standalone execute the test function main_test
if __name__ == '__main__':
    main_test()


