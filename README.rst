Stereo tracker with two USB cams
======================

A basic example of optical tracking, stereo vision, sockets and OOP for Poletni Tabor Invoacijskih Tehnologij


Installation:
-------------
This project is tested only under python2.7.

Installing opencv:
..................

Use conda packet manager:  `conda install -c menpo opencv3`


Installing PySide
.................

If under conda use `conda install pyside`

Running
-------

Once the above dependencies are installed
`python OOPApp/TrackingServer.py`


How to calibrate:
-----------------

The easiest way now is a follows:

1. Remove the *.calibrate files

2. Run StereoCamera.py //namely test number 0

3.1 Show a checkerboard and press "a" to acquire an image

3.2 Press "h" to confirm that corner detection is of sufficient quality

3.3 Repeat 3.1 and 3.2 10+ times and press "c" to calibrate a single camera

4. Repeat the entire point 3 for another camer

5.1 Now calibrate the relative position of cameras:
show checkerboard in such a way that it's visible in both cameras and press "a"

5.2 When enough (20+) frames are acquired press "c" to calibrate.

5.3 Press "ESC" to finish the process




