__author__ = 'Gena'
'''
stereo calibration app for FE Summer School
'''

class calibration_app(object):
    self.controls_window_name = 'Control Panel' #STATIC, a very serious name
    self.exposure = [-11,-11] #Camera exposure controls

    def create_controls(self):
        cv2.namedWindow(self.controls_window_name)
        cv2.createTrackbar("Exposure",self.controls_window_name,int(-self.cam.get(cv2.CAP_PROP_EXPOSURE)),11,lambda x: self.cam.set(cv2.CAP_PROP_EXPOSURE,-x))
        cv2.createTrackbar("hough_rmin",self.controls_window_name,self.hough_rmin, 255, lambda x: setattr(self,'hough_rmin',x))
        cv2.createTrackbar("hough_rmax",self.controls_window_name,self.hough_rmax, 255, lambda x: setattr(self,'hough_rmax',x))
        cv2.createTrackbar("hough_accu",self.controls_window_name,self.hough_accu, 255, lambda x: setattr(self,'hough_accu',x))
        cv2.createTrackbar("canny_low",self.controls_window_name,self.canny_low, 255, lambda x: setattr(self,'canny_low',x))
        cv2.createTrackbar("canny_hi",self.controls_window_name,self.canny_hi, 255, lambda x: setattr(self,'canny_hi',x))