__author__ = 'Gena'

import cv2
import time
from stereovision import calibration

######################## INIT #########################

ncams = 2

cap = [cv2.VideoCapture(x) for x in range(ncams)]
im = range(ncams) #init the list for later indexing
ret = im
# some spaghetization :(
calibrator = calibration.StereoCalibrator(rows= 6, columns = 9,square_size = 2.54, image_size = (640, 480))


while 1:
    for i in range(ncams):
        #quickly grab a frame fro each camera
        ret[i] = cap[i].grab()


    for i in range(ncams):
        # retrieve the grabbed images
        if ret[i]:
            ret[i], im[i] = cap[i].retrieve()
        else:
            raise IOError('could not grab image from camera')

        cv2.imshow(str(i),im[i])
    ch = cv2.waitKey(1)
    if ch == ord('q'):
        break
    elif ch == ord('c'):
        try:
            calibrator.add_corners((im[0],im[1]),show_results=True)
            # temp = image
            # cv2.drawChessboardCorners(temp, (self.rows, self.columns), corners,
            #                           True)
            # window_name = "Chessboard"
            # cv2.imshow(window_name, temp)
            time.sleep(1)
        except calibration.ChessboardNotFoundError:
            print 'no chessboard pattern present'

############## THIS WILL FAIL IN OPENCV3!!! ###########
# TODO: FIX YE BUG IN stereovision MODULE
calibration_object = calibrator.calibrate_cameras()
############## END WILL FAIL ##########################

#################### CALIBRATE  EACH CAMERA  THOU SHALTH #########################
side = ['left','right']
rms = [0,1]
camera_matrix = [0,1]
dist_coefs = [0,1]
rvecs = [0,1]
tvecs = [0,1]
for i in (0,1):
    rms[i], camera_matrix[i], dist_coefs[i], rvecs[i], tvecs[i] = cv2.calibrateCamera(calibrator.object_points, calibrator.image_points[side[i]], calibrator.image_size, None, None)

for i in (0,1):
    print "RMS:", rms[i]
    print "camera matrix:\n", camera_matrix[i]
    print "distortion coefficients:\n ", dist_coefs[i].ravel()

####################### BEHOLD THE DIFFERENCE ######################################
''' Both camrahs are teh saem make. SO the parm diff should be smalleh'''

print 'camra matrix diff:\n', camera_matrix[1]-camera_matrix[0]
print 'distcof diff:\n', dist_coefs[1]-dist_coefs[0]

print ' I PRINTAH!'

############################ UNDISTORTAH!!! #################################

undis = cv2.undistort(src= im[0], cameraMatrix = camera_matrix,distCoeffs = dist_coefs)
cv2.imshow('undis',undis)
